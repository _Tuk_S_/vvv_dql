<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Dish;
use AppBundle\Entity\Purchase;
use AppBundle\Entity\Shop;
use AppBundle\Form\BasketType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SiteController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $shops = $this->getDoctrine()
            ->getRepository('AppBundle:Shop')
            ->findAll();
        $user = $this->getUser();

        $popular_dishes = [];
        $date = (new \DateTime(date("Y")."-01-01"))->format("Y-m-d");

        foreach ($shops as $shop) {
            $popular_dishes[] = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Purchase')
                ->getPopularDishes($shop, $date);

        }

        return $this->render('AppBundle:Site:index.html.twig', array(
            'shops' => $shops,
            'user' => $user,
            'popular_dishes' => $popular_dishes
        ));
    }

    /**
     * @Route("/shop/{shop_id}")
     * @param int $shop_id
     * @return Response
     */
    public function detailsShopAction(int $shop_id)
    {
        $shop = $this->getDoctrine()->getRepository(Shop::class)->find($shop_id);
        $dishes = $shop->getDishes();
        $user = $this->getUser();

        $cart_form = $this->createForm(BasketType::class);
        $total = 0;
        $shop_items = null;

        $session = $this->get('session');

        if ($session->get('basket')) {
            $items = $session->get('basket');

            foreach ($items as $item) {
                if ($item['id'] == $shop_id) {
                    $shop_items[] = $item;
                    $total += $item['sum'];
                }
            }
        } else {
            $items = null;
        }

        return $this->render('@App/Site/shop.html.twig', array(
            'shop'      => $shop,
            'dishes'    => $dishes,
            'user'      => $user,
            'cart_form' => $cart_form,
            'items'     => $items,
            'total'     => $total,
            'shop_items'=> $shop_items

        ));
    }

    /**
     * @Route("/add-{dish_id}")
     * @Method("POST")
     * @param Request $request
     * @param int $dish_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addToBasketAction(Request $request, int $dish_id)
    {
        $dish = $this->getDoctrine()->getRepository(Dish::class)->find($dish_id);
        $array = $request->request->get('app_bundle_basket_type');

        if ($array['qty'] < 0) {
            $qty = 0;
        } else {
            $qty = $array['qty'];
        }

        $this->savePurchaseAction($dish, $qty);

        $basket = $request->getSession()->get('basket', []);
        array_push($basket, [
            'id' => $dish->getShop()->getId(),
            'name' => $dish->getName(),
            'qty'  => $qty,
            'sum'  => $qty * $dish->getPrice()
        ]);
        $request->getSession()->set('basket', $basket);

        return $this->redirectToRoute('app_site_detailsshop', array('shop_id' => $dish->getShop()->getId()));
    }

    /**
     * @param $dish
     * @param $qty
     */
    public function savePurchaseAction($dish, $qty): void
    {
        $order = new Purchase();
        $order->setDish($dish);
        $order->setQty($qty);
        $order->setOrderDate(new \DateTime());

        $em = $this->getDoctrine()->getManager();

        $em->persist($order);
        $em->flush();
    }
}
